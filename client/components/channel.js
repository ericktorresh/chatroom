// ======================
// Channels list
// ======================
Template.channels.helpers({
	channels: function () {
		return Channels.find();
	}
});

Template.channel.helpers({
	isActive: function()
	{
		return Session.get('channel') == this._id;
	}
});

Template.channels.events({
	'submit .add-channel-form': function (event){
		// Prevent default browser form submit
		event.preventDefault();

		// Get value from form element
		var $name = $(event.target.name);

		Meteor.call("addChannel", $name.val(), function(error, channelId){
			console.log(channelId);

			if (typeof channelId !== 'undefined')
				Router.go('messagesList', {_id :channelId});
		});

		//Clear form
		$name.val('');
	}
});