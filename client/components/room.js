var enableTracker = false;

var scrollRoom = function(animate){
	var d = $('.messages-wrapper .collection');
	if (animate == true) d.animate({ scrollTop: d[0].scrollHeight}, 1000);
	else d.scrollTop(d.prop("scrollHeight"))
}

// ======================
// Message list
// ======================
Template.messagesList.helpers({
	messages: function (channel) {
		var messages = Messages.find({channel : channel});
		// Observers
		/*var handle = messages.observeChanges({
			added: function (id, user) {
				console.log('add')
			},
			removed: function () {
				console.log('Removed');
			}
		});*/
		return messages;
	}
});

Template.messagesList.onRendered(function(){

	// Update each 30 seconds dates
	setInterval(function(){
		console.log('run');
		var dates = $('[data-date]');
		$.each(dates, function(i,o){
			var $self = $(o);
			if (typeof $self.data('date') !== 'undefined')
			{
				var d = new Date($self.data('date'));

				if (moment(d).isValid())
				{
					$self.text(moment(d).fromNow());
				}
			}
		});
	}, 30000);

	//console.log(Router.current().messageHandle);
});


// 
// Tracker to apply scroll into room
// 
Tracker.autorun(function (c) {
	if (Session.equals("messagesReady", true)){
		scrollRoom(false);
		//Session.set("messagesReady", false);
	}
});

Tracker.autorun(function (c) {
	var count = Messages.find().count();
	if (count > 0){
		scrollRoom(false);
	}
});


// ======================
// Message Item helper
// ======================
Template.message.helpers({
	isOwner: function ()
	{
		return this.userId === Meteor.userId();
	}
});

Template.message.events({
	'click .edit-message' : function()
	{
		$(Template.instance().firstNode).addClass('editing');
		Session.set('selectedId', this._id);
		$('.new-message input').focus();
	},
	'click .delete-message' : function()
	{
		$(Template.instance().firstNode).addClass('editing');

		if (confirm('Are you sure?'))
		{
			Meteor.call("removeMessage", this._id, function(error, result){
				if (error) console.log(error);
			});
		}

		$('.editing').removeClass('editing');
	}
});

// ======================
// Form events
// ======================
Template.messageForm.events({
	'focus .new-message input' : function(event){
		var $input = $(event.currentTarget);

		if (typeof Session.get('selectedId') != 'undefined')
		{
			var currentMessage = Messages.findOne({
				_id : Session.get('selectedId'),
				userId : Meteor.userId()
			});

			if (typeof currentMessage === 'undefined'){
				alert('Unauthorized');
				event.preventDefault();
				return;
			}

			$input.val(currentMessage.text);
			$input.data('id', currentMessage._id);

			// delete key from session object
			delete Session.keys['selectedId'];
		}	
	},

	'blur .new-message input' : function(event){
		var $input = $(event.currentTarget);
		if ($.trim($input.val()).length == 0)
		{
			if (typeof $input.data('id') != 'undefined')
			{
				delete Session.keys['selectedId'];
				$input.removeData('id');
				$('.editing').removeClass('editing');
			}
		}
	},

	'submit .new-message': function (event){
		// Prevent default browser form submit
		event.preventDefault();
		// Get value from form element
		var $text = $(event.target.text);

		if ($.trim($text.val()).length == 0)
		{
			return;
		}
		
		if (typeof $text.data('id') == 'undefined')
		{
			// New message
			Meteor.call("addMessage", {
				text : $text.val(), 
				channel: Session.get('channel'), 
				created_dt:  moment().format('YYYY-MM-DD HH:mm:ss'),
				updated_dt: moment().format('YYYY-MM-DD HH:mm:ss')
			}, function(error, result){
				//console.log(error);
			});
		}
		else
		{
			// Edit message
			Meteor.call("editMessage",  $text.data('id'), {
				text: $text.val(),
				updated_dt: moment().format('YYYY-MM-DD HH:mm:ss')
			}, function(error, result){
				//console.log(error);
			});
			// Clear data to prevent future editing
			$text.removeData('id');
		}

		//Clear form
		$text.val('');
		$('.editing').removeClass('editing');
	}
});