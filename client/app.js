// App client config
Accounts.ui.config({
	passwordSignupFields: 'USERNAME_AND_EMAIL'
});

// Helper
Template.registerHelper("userById", function (userId) {
	var user = Meteor.users.findOne({_id: userId});

	if (typeof user === "undefined") 
	{
		return 'Anonymous';
	}

	return user.username;
});

Template.registerHelper("humanizeDate", function (dateString) {
	if (!moment(dateString).isValid())
	{
		return '';
	}
	return moment(dateString).fromNow();
});