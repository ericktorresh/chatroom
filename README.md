## README File

This project was developed with the purpose to introduce to MeteorJS.
This app is a realtime chat.

###Installation
* Install meteor in your pc. [Install guide](https://www.meteor.com/install)
* Install bower in your computer
* In the folder path, run `bower install`
* Deploy meteor in your project root using: `meteor`