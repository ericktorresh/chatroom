Meteor.methods({
	addMessage: function (message)
	{
		check(message.text, String);
		check(message.channel, String);

		if (! Meteor.userId()) throw new Meteor.Error("not-authorized");
		
		// Insert a task into the collection
		Messages.insert({
			text: message.text,
			channel: message.channel,
			edited: false,
			userId: Meteor.userId(),
			created_dt: message.created_dt, // current time
			updated_dt: message.updated_dt // current time
		});
	},

	editMessage: function (id, message)
	{
		check(id, String);
		check(message.text, String);

		var currentMessage = Messages.findOne({_id : id});

		if (typeof currentMessage == 'undefined')
		{
			throw new Meteor.Error("Message not found!");
		}

		if (!Meteor.userId() || currentMessage.userId != Meteor.userId()) 
		{
			throw new Meteor.Error("not-authorized");
		}

		Messages.update(currentMessage._id, {
			$set: {
				text : message.text,
				edited : true,
				updated_dt: message.updated_dt
			}
		}, function(error) {
			if (error) {
				throw new Meteor.Error(error);
			}
		});
	},

	removeMessage: function(id)
	{
		var currentMessage = Messages.findOne({_id : id});
		
		if (typeof currentMessage == 'undefined')
		{
			throw new Meteor.Error("Message not found!");
		}

		if (!Meteor.userId() || currentMessage.userId != Meteor.userId()) 
		{
			throw new Meteor.Error("not-authorized");
		}

		Messages.remove(id);
	},

	// Channel
	addChannel: function (name)
	{
		check(name, String);

		if (! Meteor.userId()) throw new Meteor.Error("not-authorized");

		return Channels.insert({
			name: name,
			userId: Meteor.userId(),
			created_dt: moment().format('YYYY-MM-DD HH:mm:ss'), // current time
		});
	},
	
});
