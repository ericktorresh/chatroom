Meteor.startup(function () {

	if (Meteor.users.find().count() === 0)
	{
		Accounts.createUser({
			'username' : 'test',
			'password' : 'test123',
			'email' : 'test@mail.com'
		});
	}

	if (Messages.find().count() === 0)
	{
		Channels.remove({});

		Channels.insert({name: 'general'});

		for(var i = 0; i < 4;i++)
		{
			Messages.insert({
				text : Fake.sentence(),
				edited : false,
				userId : Meteor.users.findOne()._id,
				channel : Channels.findOne()._id,
				created_dt : moment().format('YYYY-MM-DD HH:mm:ss'),
				updated_dt : moment().format('YYYY-MM-DD HH:mm:ss') // current time
			});
		}
	}
});