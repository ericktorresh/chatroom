Meteor.publish('channels', function(){
	return Channels.find();
});

Meteor.publish('messages', function(channelId){
	return Messages.find({channel: channelId});
});

Meteor.publish("allUsers", function () {
	return Meteor.users.find();
});