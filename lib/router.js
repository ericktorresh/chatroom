Router.configure({
	layoutTemplate: 'app',
	waitOn: function() {
		return [Meteor.subscribe('channels'), Meteor.subscribe('allUsers')];
	}
});
 
Router.map(function() {
	this.route('messagesList', {
		path: '/room/:_id',
		onBeforeAction: function () {
			this.messageHandle = this.subscribe('messages', this.params._id, function(){
				Session.set('messagesReady', true);
			});

			if (this.ready()) {
				this.next();
			}

			Session.set('channel', this.params._id);
		},
		data: function () {
			return Channels.findOne({_id : this.params._id});
		},
		action: function () {
			this.render();
		}
	});

	this.route('home', {
		path: '/',
		action: function() {
			Router.go('messagesList', Channels.findOne());
		}
	});
});